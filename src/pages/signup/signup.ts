import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthenticationProvider } from '../../services/api/authentication';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  user = {name:'', email:'', confirmEmail:'',password:'',confirmPassword:''}

  submitted = false;

  constructor(public navCtrl: NavController, public api: AuthenticationProvider) {
    console.log(this.user);
  }

  onSubmit(){
    console.log(this.user);

    this.api.postAccount(this.user)
    .then(data => {
      console.log('signup',data);
    })
    .catch(err => {
       console.log('signup error',err)
    })
  }

  back(){
    this.navCtrl.pop();
  }
}
